let users = 
{
	"username": 'johndoe',
	"password": 'johndoe123'
}

const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));


app.get('/home', (request, response) => {
	response.send('Welcome to the Homepage!')
})

app.post('/signup', (request, response) => {
	
	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
		console.log(request.body);
	}
	else {
		response.send(`Please input BOTH username and password`);
	}
})

app.get('/users', (request, response) => {
	response.send(users)
})

app.delete('/delete-user', (request, response) => {
	response.send(`The user has been deleted`)
})

app.listen(port, () => console.log(`Server running at port ${port}.`));










